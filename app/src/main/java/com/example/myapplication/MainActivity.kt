package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*

class MainActivity : AppCompatActivity() {

    private lateinit var email: EditText
    private lateinit var nextButton: Button
    private lateinit var saxeli : EditText
    private lateinit var gvari : EditText
    private lateinit var check_box : CheckBox
    private lateinit var paroli : EditText
    private lateinit var register : TextView




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        email = findViewById(R.id.emailid)
        register = findViewById(R.id.regid)
        check_box = findViewById(R.id.checkBox)
        paroli = findViewById(R.id.passwordid)
        nextButton = findViewById(R.id.button)
        saxeli = findViewById(R.id.saxeli)
        gvari = findViewById(R.id.gvari)


        nextButton.setOnClickListener(){
            if (saxeli.text.isEmpty() || saxeli.text.length > 16) {
                Toast.makeText(
                    this,
                    "გთხოვთ შეიყვანოთ თქვენი სახელი. იგი არ უნდა აღემატებოდეს 16 სიმბოლოს",
                    Toast.LENGTH_SHORT
                ).show()
            }
            else if (gvari.text.isEmpty()){
                Toast.makeText(this, "თქვენ არ შეგიყვანიათ გვარი", Toast.LENGTH_SHORT).show()
            }

            else if (email.text.isEmpty()){
                Toast.makeText(this, "თქვენ არ შეგიყვანიათ თქვენი Email", Toast.LENGTH_SHORT).show()
            }
            else if (paroli.text.isEmpty()){
                Toast.makeText(this, "გთხოვთ შეიყვანოთ პაროლი ", Toast.LENGTH_SHORT).show()
            }
            else if (check_box.isChecked==false){
                Toast.makeText(this, "გთხოვთ დაეთანხმოთ წესებსა და პირობებს", Toast.LENGTH_SHORT).show()
            }
            else if (check_box.isChecked){
                Toast.makeText(this , "თქვენ წარმატებით გაიარეთ რეგისტრაცია", Toast.LENGTH_SHORT).show()
            }

        }

    }



}